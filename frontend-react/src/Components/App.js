/* eslint-disable no-unused-vars */
import React, { Component } from 'react'
import Header from './Header'
import Main from './Main'
import ipfs from '../Utils/ipfs'
import web3 from '../Utils/web3'

class App extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {buffer: null, image: 'hola'}
//     }

    //     async componentDidMount() {
    //     // console.log(await ipfs.files.getReadableStream('QmRe8Tusp72nC5PVcvgQeudqfAhQYWcEmQFbkQABJWdNSm'))

    //     }

    //     handleClick() {
    //     // this.setState({ test: 'ha' })

    //         // QmRe8Tusp72nC5PVcvgQeudqfAhQYWcEmQFbkQABJWdNSm

    //     // let test = await ipfs.files.get('QmRe8Tusp72nC5PVcvgQeudqfAhQYWcEmQFbkQABJWdNSm')
    //     // console.log(test[0].content.toString('utf8'))
    //     // const img = test[0].content.toString("base64");
    //     // const imgFinal = "data:image/png;base64," + img
    //     // console.log(imgFinal)
    //     // this.setState({ test: 'ha' })
    //     // ipfs.add('test', (err, ipfsHash) => {
    //     //   console.log(err, ipfsHash)
    //     // })
    //     }

    //   captureFile = (event) => {
    //       event.stopPropagation()
    //       event.preventDefault()
    //       // console.log(event.target.files)

    //       const file = event.target.files[0]
    //       // this.setState({image: URL.createObjectURL(file)})
    //       let reader = new window.FileReader()
    //       reader.readAsArrayBuffer(file)
    //       reader.onloadend = () => this.convertToBuffer(file, reader)
    //   };

    //   convertToBuffer = async (file, reader) => {
    //       // eslint-disable-next-line no-undef
    //       const buffer = await Buffer.from(reader.result)
    //       this.setState({buffer})
    //       console.log(file)
    //       console.log(buffer)
    //       // console.log(Buffer)
    //   };

    //   onSubmit = async (event) => {
    //       event.preventDefault()
    //       const accounts = await web3.eth.getAccounts()
    //       console.log('Sending from Metamask account: ' , accounts[0])
    //       // const ethAddress= await storeMyValue.options.address;
    //       // this.setState({ethAddress});
    //       console.log(this.state.buffer)
    //       await ipfs.files.add(this.state.buffer, async (err, ipfsHash) => {
    //           console.log(err,ipfsHash)
    //           this.setState({ ipfsHash:ipfsHash[0].hash })
    //           let images = await ipfs.files.get(this.state.ipfsHash)
    //           const img = images[0].content.toString('base64')
    //           const imgFinal = 'data:image/png;base64,' + img
    //           this.setState({image: imgFinal})
    //       // storeMyValue.methods.set(this.state.ipfsHash).send({
    //       // from: accounts[0]
    //       // }, (error, transactionHash) => {
    //       // console.log("transaction hash is ",transactionHash);
    //       // this.setState({transactionHash});
    //       // });
    //       })
    //   };

    render() {
        return (
            <div>
                <Header />
                <Main />
            </div>
        )
        //   return (
        //       <div className="App">
        //           <header className="App-header">
        //               <img src={this.state.image} alt="test" />
                //       <form onSubmit={this.onSubmit}>
                //           <input
                //               type = "file"
                //               onChange = {this.captureFile}
                //           />
                //           <Button
                //               variant="contained"
                //               color="primary"
                //               type="submit">
                //  Send it
                //           </Button>
                //       </form>
        //               <Button variant="contained" color="primary" onClick={this.handleClick}>
        //         Upload Picture
        //               </Button>
        //           </header>
        //       </div>
        //   )
    }
}

export default App
