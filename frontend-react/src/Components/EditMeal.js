import React, { Component } from 'react'
import web3 from '../Utils/web3'
import Button from '@material-ui/core/Button'
import contractInstance from '../Utils/Contract'
import ReactLoading from 'react-loading'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import ipfs from '../Utils/ipfs'

class EditMeal extends Component {
    constructor(props){
        super(props)
        this.state = { image: null, buffer: null, mealId: props.match.params.number, meal: null, processing: false, account: null, date: new Date() }
    }

    async componentDidMount(){
        const account = await web3.eth.getAccounts()

        const meal = {}
        if(this.state.mealId !== '_'){
            const mealPreview = await contractInstance.methods.meals(this.state.mealId).call()
        
            meal.capacity = parseInt(mealPreview.capacity, 10)
            meal.cook = mealPreview.cook
            meal.description = mealPreview.description
            meal.location = mealPreview.location
            meal.mealId = mealPreview.mealId
            meal.price = mealPreview.price
            meal.time = mealPreview.time
            meal.title = mealPreview.title
    
            const reservations = await contractInstance.methods.totalReservation(meal.mealId).call()
            meal.reservations = reservations.length
        }
        
        this.setState({ meal, account: account[0]})
    }

    editMeal = async () => {
        const { account, meal } = this.state
        this.setState({ processing: true})
        if(this.state.mealId !== '_'){
            contractInstance.methods.changeMeal(meal.mealId, meal.title, meal.description, meal.location, meal.capacity).send({from: this.state.account})
                .on('confirmation', () => {
                    contractInstance.events.MealChanged()
                    window.location.href = 'http://localhost:3000/cook'
                })
                .on('error', () => {
                    alert ('Sorry! The transaction was cancelled.')
                    this.setState({ processing: false})
                })
        } else {
            meal.time = this.state.date.getTime() / 1000
            console.log('Sending from Metamask account: ' , this.state.account)
            if(this.state.buffer) {
                const ipfsHashes = await ipfs.files.add(this.state.buffer)
                this.setState({ ipfsHash: ipfsHashes[0].hash})
                contractInstance.methods.addMeal(meal.title, meal.description, meal.location, meal.time, meal.capacity, meal.price, this.state.ipfsHash).send({from: account})
                    .on('confirmation', () => {
                        contractInstance.events.MealAdded()
                        window.location.href = 'http://localhost:3000/cook'
                    })
                    .on('error', () => {
                        alert ('Sorry! The transaction was cancelled.')
                        this.setState({ processing: false})
                    })
            } else {
                contractInstance.methods.addMeal(meal.title, meal.description, meal.location, meal.time, meal.capacity, meal.price, '').send({from: account})
                    .on('confirmation', () => {
                        contractInstance.events.MealAdded()
                        window.location.href = 'http://localhost:3000/cook'
                    })
                    .on('error', () => {
                        alert ('Sorry! The transaction was cancelled.')
                        this.setState({ processing: false})
                    })
            }
            
           
        }
    }

    captureFile = (event) => {
        event.stopPropagation()
        event.preventDefault()

        const file = event.target.files[0]
        this.setState({image: URL.createObjectURL(file)})
        let reader = new window.FileReader()
        reader.readAsArrayBuffer(file)
        reader.onloadend = () => this.convertToBuffer(file, reader)
    };

    convertToBuffer = async (file, reader) => {
        // eslint-disable-next-line no-undef
        const buffer = await Buffer.from(reader.result)
        this.setState({buffer})
    };

    render() {
        const { meal, processing } = this.state
        if(!processing){
            if (meal){
                return (
                    <div className="container">
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Title*</label>
                            <div className="col-sm">
                                <input type="text" required="required" className="form-control" ref="title" value={meal.title} onChange={(event) => {
                                    meal.title = event.target.value
                                    this.setState({ meal }) 
                                }}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Description <br/> <span style={{fontSize: '0.8em'}}></span></label>
                            <div className="col-sm">
                                <textarea className="form-control" maxLength="5000" ref="description" value={meal.description} rows="3" onChange={(event) => {
                                    meal.description = event.target.value
                                    this.setState({ meal }) 
                                }}></textarea>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">When*</label>
                            <div className="col-sm">
                                <DatePicker disabled={this.state.mealId !== '_'}
                                    selected={this.state.date}
                                    onChange={(date) => {this.setState({date})}}
                                    showTimeSelect
                                    dateFormat="Pp"
                                />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label" >Where* <span style={{fontSize: '0.8em'}}>(max 25 characters)</span></label>
                            <div className="col-sm">
                                <input type="text" maxLength="25" required="required" className="form-control" ref="place" value={meal.location} onChange={(event) => {
                                    meal.location = event.target.value
                                    this.setState({ meal }) 
                                }}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Capacity*</label>
                            <div className="col-sm">
                                <input type="number" min="1" step="1" className="form-control" required="required" ref="capacity" pattern="^[1-9]" value={meal.capacity} onChange={(event) => {
                                    meal.capacity = event.target.value
                                    this.setState({ meal }) 
                                }}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Price (Eth)</label>
                            <div className="col-sm">
                                <input type="text" className="form-control" ref="price" required="required"  value={meal.price} disabled={this.state.mealId !== '_'} onChange={(event) => {
                                    meal.price = event.target.value
                                    this.setState({ meal }) 
                                }}/>
                            </div>
                        </div>

                        <img src={this.state.image} alt={'test'} width={'150px'} />
                        
                        <div className="form-group row">
                            <input
                                type = "file"
                                onChange = {this.captureFile}
                            />
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => this.setState({ image: null, buffer: null })}
                            >
                                Remove
                            </Button>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm text-right">
                                <Button style={{margin: '10px'}} variant="contained" color="secondary" onClick={() => window.history.back()}>Cancel</Button>       
                                <Button style={{margin: '10px'}} variant="contained" color="primary" onClick={this.editMeal}>Save</Button>
                            </div>
                        </div>
                    </div>
                )
            }
            return (
                <div>
                </div>
            )
        } else {
            return (
                <div className="container">
                    <ReactLoading type={'balls'} color={'blue'} height={'20%'} width={'20%'} />
                </div>
            )
        }
        
    }
}

export default EditMeal
