import React, { Component } from 'react'
import web3 from '../Utils/web3'
import contractInstance from '../Utils/Contract'
import Button from '@material-ui/core/Button'
import Moment from 'react-moment'

class Cook extends Component {
    constructor(props){
        super(props)
        this.state = { meals: null, account: null, balance: 0 }
    }

    async componentDidMount() {
        const account = await web3.eth.getAccounts()
        const balance = await web3.eth.getBalance(account[0])
        const mealsLength = await contractInstance.methods.getMealsLength().call()
        const meals = []
        for (let i = 0; i < mealsLength;  i++) {
            const meal = await contractInstance.methods.meals(i).call()
            if(meal.cook === account[0]){
                meals.push({
                    capacity: meal.capacity,
                    cook: meal.cook,
                    description: meal.description,
                    location: meal.location,
                    mealId: meal.mealId,
                    price: meal.price,
                    time: meal.time,
                    title: meal.title
                })
            }
        }
        this.setState({ account: account[0], balance, meals })
    }

    render() {
        const { meals } = this.state
        if(meals){
            const times = []
            for (let i = 0; i < meals.length; i++) {
                times.push = meals[i].time
            }
            return(
                <div className="container">
                    <div className="mb-3">
                        <b>Your Balance: {web3.utils.fromWei(this.state.balance)} Eth</b>
                    </div>

                    {meals.length > 0 && 
                    <div>
                        <div className="row mb-3">
                            <div className="col vcenter">
                                Here is a list of the meals you are currently offering. Click on a meal to edit it.
                            </div>
                        </div>
                        
                        <div className="row">
                            <div className="col-sm vcenter"><b>What</b></div>
                            <div className="col-sm vcenter"><b>When</b></div>
                            <div className="col-sm vcenter"><b>Where</b></div>
                            <div className="col-sm-2 vcenter"><b>Price</b></div>
                        </div>

                        {this.state.meals.map(meal => (
                            <div key={meal.mealId}>
                                <hr />
                                <div className="row">
                                    <div className="col-sm vcenter">
                                        <a href={'meal/' + meal.mealId + '/edit'}><b>{meal.title}</b>
                                            {this.state.account === meal.cook &&
                                            <b> (owner)</b>
                                            }
                                        </a>
                                    </div>
                                    <div className="col-sm vcenter">
                                        <Moment>{times[meal.mealId]}</Moment>
                                    </div>
                                    <div className="col-sm vcenter">
                                        {meal.location}
                                    </div>
                                    <div className="col-sm-2 vcenter">
                                        {web3.utils.fromWei(meal.price)} Eth
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                    }

                    {meals.length === 0 &&
                    <div>
                        <div>
                            <p>
                                You are not offering any meals at the moment.
                            </p>
                        </div>
                    </div>
                    }
                    <div className="row mt-3">
                        <div className="col-sm text-center">
                            <a href={'meal/_/edit'}>Offer a new meal</a>
                        </div>
                    </div>


                </div>
            )
        }
        return (
            <div></div>
        )
    }
}

export default Cook
