import React, { Component } from 'react'
import web3 from '../Utils/web3'
import contractInstance from '../Utils/Contract'
import Moment from 'react-moment'
// import ReactLoading from 'react-loading'

class Eat extends Component {
    constructor(props) {
        super(props)
        this.state = { account: null, balance: 0, meals: null }
    }

    async componentDidMount() {
        const account = await web3.eth.getAccounts()
        const balance = await web3.eth.getBalance(account[0])
        const mealsLength = await contractInstance.methods.getMealsLength().call()
        const meals = []
        for (let i = 0; i < mealsLength;  i++) {
            const meal = await contractInstance.methods.meals(i).call()
            meals.push({
                capacity: meal.capacity,
                cook: meal.cook,
                description: meal.description,
                location: meal.location,
                mealId: meal.mealId,
                price: meal.price,
                time: meal.time,
                title: meal.title
            })
        }
        this.setState({ account: account[0], balance, meals })
    }

    upcomingMealsText() {
        if (this.state.meals.length > 0){
            return (
                <div>
                    <p>List of upcoming meals. Click on a meal to make a reservation.</p>
                </div>
            )
        }
        return (
            <div>
                <p>There are no upcoming meals.</p>
            </div>
        )
    }

    render() {
        if (this.state.meals) {
            const times = []
            for (let i = 0; i < this.state.meals.length; i++) {
                times.push = this.state.meals[i].time
            }
            return (
                <div className="container">
                    <div className="mb-3">
                        <b>Your Balance: {web3.utils.fromWei(this.state.balance)} Eth</b>
                    </div>
                    
                    {this.upcomingMealsText()}

                    <div className="row">
                        <div className="col-sm vcenter"><b>What</b></div>
                        <div className="col-sm vcenter"><b>When</b></div>
                        <div className="col-sm vcenter"><b>Where</b></div>
                        <div className="col-sm-2 vcenter"><b>Price</b></div>
                    </div>

                    {this.state.meals.map(meal => (
                        <div key={meal.mealId}>
                            <hr />
                            <div className="row">
                                <div className="col-sm vcenter">
                                    <a href={'meal/' + meal.mealId}><b>{meal.title}</b>
                                        {this.state.account === meal.cook &&
                                            <b> (owner)</b>
                                        }
                                    </a>
                                </div>
                                <div className="col-sm vcenter">
                                    <Moment>{times[meal.mealId]}</Moment>
                                </div>
                                <div className="col-sm vcenter">
                                    {meal.location}
                                </div>
                                <div className="col-sm-2 vcenter">
                                    {web3.utils.fromWei(meal.price)} Eth
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}

export default Eat
