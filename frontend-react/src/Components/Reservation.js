import React, { Component } from 'react'
import web3 from '../Utils/web3'
import ipfs from '../Utils/ipfs'
import Button from '@material-ui/core/Button'
import contractInstance from '../Utils/Contract'
import Moment from 'react-moment'
import ReactLoading from 'react-loading'

class Reservation extends Component {
    constructor(props){
        super(props)
        this.state = { mealId: props.match.params.number, meal: null, processing: false }
    }

    async componentDidMount(){
        const meal = {}
        const mealPreview = await contractInstance.methods.meals(this.state.mealId).call()
        
        meal.capacity = parseInt(mealPreview.capacity, 10)
        meal.cook = mealPreview.cook
        meal.description = mealPreview.description
        meal.location = mealPreview.location
        meal.mealId = mealPreview.mealId
        meal.price = mealPreview.price
        meal.time = mealPreview.time
        meal.title = mealPreview.title
        const reservations = await contractInstance.methods.totalReservation(meal.mealId).call()
        meal.reservations = reservations.length

        if(mealPreview.imageHash !== '') {
            let images = await ipfs.files.get(mealPreview.imageHash)
            meal.imageHash = 'data:image/png;base64,' + images[0].content.toString('base64')
        }

        this.setState({ meal })
    }

    reservationsAvailable() {
        const { meal } = this.state
        if (meal.reservations === meal.capacity){
            return (
                <p>
                    <span style={{color: 'red'}}><b>Number of reservations: </b>{meal.reservations} / {meal.capacity} (SOLD OUT)</span>
                </p>
            )
        }
        return (
            <div>
                <p>
                    <span style={{color: 'green'}}><b>Number of reservations: </b>{meal.reservations} / {meal.capacity} (Available)</span>
                </p>
            </div>
        )
    }

    joinMeal = async () => {
        const account = await web3.eth.getAccounts()
        const balance = await web3.eth.getBalance(account[0])
        const { meal } = this.state
        if (meal.reservations < meal.capacity) {
            if (parseInt(web3.utils.fromWei(balance, 'ether')) >= parseInt(web3.utils.fromWei(meal.price, 'ether'))) {
                this.setState({ processing: true })
                contractInstance.methods.addReservations(meal.mealId).send({gas:3000000, from: account[0], value: meal.price})
                    .on('confirmation', () => {
                        contractInstance.events.MealReservationChange()
                        window.location.href = 'http://localhost:3000'
                    })
                    .on('error', () => {
                        alert ('Sorry! The transaction was cancelled.')
                        this.setState({ processing: false})
                    })
            } else {
                alert ('Sorry! You do not have enough balance. Current balance: ' + parseInt(web3.utils.fromWei(balance, 'ether')) +' - Price: '+ parseInt(web3.utils.fromWei(meal.price, 'ether')))
            }
        } else {
            alert ('Sorry! This meal has been sold out.')
        }
    }

    render() {
        const { meal, processing } = this.state

        if(!processing){
            if (meal){
                return (
                    <div className="container">
                        <h1>{meal.title}</h1>
                        <p>{meal.description}</p>
                        <p><img src={meal.imageHash} style={{height: '100px'}}></img></p>
                        <p><b>Time: </b>{meal.time}</p>
                        <p><b>Place: </b>{meal.location}</p>
                        {this.reservationsAvailable()}
                        <p><b>Price: </b>{web3.utils.fromWei(meal.price)} Eth</p>
                        <Button variant="contained" color="primary" onClick={this.joinMeal}>
                            Join
                        </Button>
                    </div>
                )
            }
            return (
                <div>
                </div>
            )
        } else {
            return (
                <div className="container">
                    <ReactLoading type={'balls'} color={'blue'} height={'20%'} width={'20%'} />
                </div>
            )
        }
        
    }
}

export default Reservation
