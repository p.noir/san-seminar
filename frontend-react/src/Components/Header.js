import React, { Component } from 'react'
// import './Styles/Header.css'
import web3 from '../Utils/web3'

class Header extends Component {
    async componentDidMount() {
        await web3.eth.getAccounts()
        window.ethereum.on('accountsChanged', function (accounts) {
            window.location.reload()
        })
    }
    render() {
        return (
            <nav className="navbar navbar-light bg-light" style={{ marginBottom: '25px' }}>
                <div className="container">
                    <div className="navbar-brand"><b>Eindhoven Catering App</b></div>
                    <div className="nav">
                        <a className="nav-item nav-link active" href="/eat">Eat</a>
                        <a className="nav-item nav-link" href="/cook">Cook</a>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header
