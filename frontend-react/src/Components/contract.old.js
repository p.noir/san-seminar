
/*
UNUSED
*/

/*
 * MetaMask will inject a web3.js component automatically
 * It will inject web3 version 0.2.x, which is documented at:
 * https://github.com/ethereum/wiki/wiki/JavaScript-API
 */


if (typeof web3 == 'undefined') {
    console.error('No web3 detected. Make sure to use a browser which injects web3, such as the Brave browser.')
  //web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
} else {
    //web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    web3 = new Web3(web3.currentProvider)
    console.log('web3 version', web3.version.api)
}

//TODO: [Nice-to-Have] Change the following line to read the contract from the file
abi = [
    {
        'constant': false,
        'inputs': [
            {
                'name': 'title',
                'type': 'string'
            },
            {
                'name': 'description',
                'type': 'string'
            },
            {
                'name': 'location',
                'type': 'string'
            },
            {
                'name': 'time',
                'type': 'uint256'
            },
            {
                'name': 'capacity',
                'type': 'uint256'
            },
            {
                'name': 'price',
                'type': 'uint256'
            }
        ],
        'name': 'addMeal',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
    },
    {
        'constant': false,
        'inputs': [
            {
                'name': 'mealId',
                'type': 'uint256'
            }
        ],
        'name': 'addReservations',
        'outputs': [],
        'payable': true,
        'stateMutability': 'payable',
        'type': 'function'
    },
    {
        'constant': false,
        'inputs': [
            {
                'name': 'mealId',
                'type': 'uint256'
            },
            {
                'name': 'title',
                'type': 'string'
            },
            {
                'name': 'description',
                'type': 'string'
            },
            {
                'name': 'location',
                'type': 'string'
            },
            {
                'name': 'capacity',
                'type': 'uint256'
            }
        ],
        'name': 'changeMeal',
        'outputs': [],
        'payable': false,
        'stateMutability': 'nonpayable',
        'type': 'function'
    },
    {
        'anonymous': false,
        'inputs': [
            {
                'indexed': false,
                'name': 'index',
                'type': 'uint256'
            }
        ],
        'name': 'MealCookChanged',
        'type': 'event'
    },
    {
        'anonymous': false,
        'inputs': [
            {
                'indexed': false,
                'name': 'index',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'price',
                'type': 'uint256'
            }
        ],
        'name': 'MealPriceChanged',
        'type': 'event'
    },
    {
        'anonymous': false,
        'inputs': [
            {
                'indexed': false,
                'name': 'index',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'price',
                'type': 'uint256'
            }
        ],
        'name': 'MealAvailabilityChanged',
        'type': 'event'
    },
    {
        'anonymous': false,
        'inputs': [
            {
                'indexed': false,
                'name': 'mealId',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'reservations',
                'type': 'address[]'
            }
        ],
        'name': 'MealReservationChange',
        'type': 'event'
    },
    {
        'anonymous': false,
        'inputs': [
            {
                'indexed': false,
                'name': 'mealId',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'cook',
                'type': 'address'
            },
            {
                'indexed': false,
                'name': 'title',
                'type': 'string'
            },
            {
                'indexed': false,
                'name': 'description',
                'type': 'string'
            },
            {
                'indexed': false,
                'name': 'location',
                'type': 'string'
            },
            {
                'indexed': false,
                'name': 'capacity',
                'type': 'uint256'
            }
        ],
        'name': 'MealChanged',
        'type': 'event'
    },
    {
        'anonymous': false,
        'inputs': [
            {
                'indexed': false,
                'name': 'mealId',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'cook',
                'type': 'address'
            },
            {
                'indexed': false,
                'name': 'title',
                'type': 'string'
            },
            {
                'indexed': false,
                'name': 'description',
                'type': 'string'
            },
            {
                'indexed': false,
                'name': 'location',
                'type': 'string'
            },
            {
                'indexed': false,
                'name': 'time',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'capacity',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'price',
                'type': 'uint256'
            },
            {
                'indexed': false,
                'name': 'reserve',
                'type': 'address[]'
            }
        ],
        'name': 'MealAdded',
        'type': 'event'
    },
    {
        'constant': true,
        'inputs': [],
        'name': 'getMealsLength',
        'outputs': [
            {
                'name': '',
                'type': 'uint256'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    },
    {
        'constant': true,
        'inputs': [
            {
                'name': 'mealId',
                'type': 'uint256'
            }
        ],
        'name': 'getReservations',
        'outputs': [
            {
                'name': '',
                'type': 'address[]'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    },
    {
        'constant': true,
        'inputs': [
            {
                'name': '',
                'type': 'uint256'
            },
            {
                'name': '',
                'type': 'uint256'
            }
        ],
        'name': 'listOfReservation',
        'outputs': [
            {
                'name': '',
                'type': 'address'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    },
    {
        'constant': true,
        'inputs': [
            {
                'name': '',
                'type': 'uint256'
            }
        ],
        'name': 'meals',
        'outputs': [
            {
                'name': 'mealId',
                'type': 'uint256'
            },
            {
                'name': 'cook',
                'type': 'address'
            },
            {
                'name': 'title',
                'type': 'string'
            },
            {
                'name': 'description',
                'type': 'string'
            },
            {
                'name': 'location',
                'type': 'string'
            },
            {
                'name': 'time',
                'type': 'uint256'
            },
            {
                'name': 'capacity',
                'type': 'uint256'
            },
            {
                'name': 'price',
                'type': 'uint256'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    },
    {
        'constant': true,
        'inputs': [
            {
                'name': '',
                'type': 'uint256'
            }
        ],
        'name': 'numberOfReservation',
        'outputs': [
            {
                'name': '',
                'type': 'uint256'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    },
    {
        'constant': true,
        'inputs': [
            {
                'name': 'mealId',
                'type': 'uint256'
            }
        ],
        'name': 'totalNumOfReservation',
        'outputs': [
            {
                'name': '',
                'type': 'uint256'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    },
    {
        'constant': true,
        'inputs': [
            {
                'name': 'mealId',
                'type': 'uint256'
            }
        ],
        'name': 'totalReservation',
        'outputs': [
            {
                'name': '',
                'type': 'address[]'
            }
        ],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function'
    }
]
/*
var fs = require('fs');
var parsed= JSON.parse(fs.readFileSync('/contract/CateringContract.json'));
var abi = parsed.abi;
*/
var myContract = web3.eth.contract(abi)
contractInstance = myContract.at('0x1429e84185c394f6ab9046c8f23b1edc5df6b8b0')

var now = (new Date()).getTime()

var _meals = {}

window.ethereum.on('accountsChanged', function (accounts) {
    init()
})

/*
 * Initialize the app:
 * - Set appName (using cache.set("appName", ...)) to a string that will be displayed in the top left corner.
 * - Set currency (using cache.set("currency", ...)) to a string that will be displayed next to the prices.
 * - Set showAddBalance (using cache.set("showAddBalance", ...)) to a boolean stating whether to show the "Add Balance" button or not.
 * - Set account (using cache.set("account", ...)) to the currently logged in wallet account.
 * - Set balance (using cache.set("balance", ...)) to the balance of the currently logged in wallet account.
 */

function init() {
    cache.set('appName', 'ECC: Eindhoven Catering Chain')
  cache.set('currency', 'ETH')
  cache.set('showAddBalance', false)
  cache.set('account', web3.eth.accounts[0])
  getBalance()
  getMeals()
} 

/*
 * Retrieve from the smart contract the list of upcoming meals.
 * Use cache.set("meals", ...) to notify the UI about the retrieved meals. 
 */

function getMeals() {
    contractInstance.getMealsLength((error, results) => {
        for (let i = 0; i < results.c[0]; i++) {
            contractInstance.meals(i, (error, results) => {
                _meals[i] = {}
				_meals[i].cook = results[1]
				_meals[i].title = results[2]
				_meals[i].description = results[3]
				_meals[i].place = results[4]
				_meals[i].time = results[5].c[0]
				_meals[i].capacity = results[6].c[0]
				_meals[i].price = (results[7]).toNumber()
				contractInstance.totalReservation(i, function(error2, result2) {
                    if (!error2) {
                        _meals[i].reservations = result2.length
						cache.set('meal', _meals[i])
					} else {
                        console.log(error2)
					}
                })
				var meals = Object.keys(_meals).map(function (id) {
                    var meal = _meals[id]
					meal.id = id
					return meal
				})
				cache.set('meals', meals)
			})
        }
    })
}

// setTimeout(function(){
// 	const node = new window.Ipfs()
// 	node.on('ready', () => {
// 		console.log('hgola')
// 	})
// }, 5000);

/*
 * Retreive from the smart contract the details about the meal with a given id.
 * Use cache.set("meal", ...) to notify the UI about the retreived meal.
 *
 * Arguments:
 *
 * - id (string): id of the meal that is to be updated
 */

function getMeal(id) {
    cache.set('meal', _meals[id])
	console.log(_meals)

    contractInstance.meals(id, function(error, result) {
        if (!error) {
			   _meals[id] =  result[0]
			   _meals[id].cook = result[1]
			   _meals[id].title = result[2]
			   _meals[id].description = result[3]
			   _meals[id].place = result[4]
			   _meals[id].time = parseInt(result[5])//1000*((result[2][i]).c[0]);
			   _meals[id].capacity = parseInt(result[6])//(result[3][i]).toString();
			   _meals[id].price = (result[7]).toNumber()
			contractInstance.totalReservation(id, function(error2, result2) {
				   if (!error2) {
					   _meals[id].reservations = result2.length
					   cache.set('meal', _meals[id])
				   } else {
					   console.log(error2)
				   }
			   })
		} else
            console.log(error)
	  })
	
}

/*
 * Create in the smart contract a new meal advertisement.
 *
 * Arguments:
 *
 * - data (object): object containing the following fields:
 *
 *     time (integer): UNIX timestamp (UTC), in milliseconds
 *     price (integer): price for the meal, in wads
 *     title (string): short description of the meal
 *     description (string): longer description of the meal
 *     place (string): where the meal will be served
 *     capacity (integer): number of meals to be served
 */

function createMeal(data) {
    let title = data.title
	let desc = data.description
	let loc = data.place
	let time = data.time
	let cap = data.capacity
	let price = data.price
	//web3.fromWei(data.price, "ether");
	//TODO: validation e.g., length and number vs string
	console.log(time)
	
    contractInstance.addMeal(title, desc, loc, time, cap, price,
		  function(error, result) {
            if (!error) {
                contractInstance.MealAdded(function(error2, result2){
					 if (!error2) {
						 getMeals()
					 } else {
						 console.log(error2)
					 }
                })
			} else
                console.log(error)
		  })
}

/*
 * Update in the smart contract the information about a meal.
 *
 * Arguments:
 *
 * - id (string): id of the meal that is to be updated
 * - data (object): object containing the following fields:
 *
 *     title (string): short description of the meal
 *     description (string): longer description of the meal
 *     place (string): where the meal will be served
 *     capacity (integer): number of meals to be served
 */

function changeMeal(id, data) {
    let title = data.title
	let desc = data.description
	let loc = data.place
	let cap = data.capacity	
	//TODO: validation e.g., length and number vs string
	
	contractInstance.changeMeal(id, title, desc, loc, cap,
		  function(error, result) {
            if (!error) {
                contractInstance.MealChanged(function(error2, result2){
					 if (!error2) {
						 console.log(result2)
						 getMeals()
					 } else {
						 console.log(error2)
					 }
                })
			} else
                console.log(error)
		  })
  
}

/*
 * Reserve a meal with the given id.
 */
//yourContractInstance.bet(7, {
//	   gas: 300000,
//	   from: web3.eth.accounts[0],
//	   value: web3.toWei(0.1, 'ether')
//	}, (err, result) => {
//	   // Result is the transaction address of that function
//	})
function reserve(id) {
    // _meals[id].reservations = (_meals[id].reservations || []).concat(cache.get("account"));	
    //TODO: validation reservation is not full
    getMeal(id)
	getBalance()
	let meal = cache.get('meal')
	let balance = cache.get('balance')
	if (meal.reservations.length < meal.capacity) {
        if (balance >= web3.fromWei(meal.price, 'ether')) {
            contractInstance.addReservations(id,
                {gas:3000000, from:web3.eth.defaultAccount, value: meal.price},
					  function(error, result) {
                    if (!error) {
                        contractInstance.MealReservationChange(function(error2, result2){
								 if (!error2) {
									 console.log(result2)
									 getBalance()
									 getMeals()
									 getMeal(id)
								 } else {
									 console.log(error2)
								 }
                        })
						} else
                        console.log(error)
					  })
		} else {
            alert ('Sorry! You do not have enough balance.<br/>Current balance: ' + balance +'<br/>Price: '+ meal.price)
        }
    } else {
        alert ('Sorry! This meal has been sold out.')
    }
}

/*
 * Add balance to the current account.
 *
 * NOTE: this function may not be necessary when integrated with a smart contract.
 */

function addBalance() {
    cache.set('balance', (cache.get('balance') || 0) + 1)
}

function getBalance() {
    web3.eth.getBalance(web3.eth.accounts[0], function(error, result) {
        if (result) {
            let balance = web3.fromWei(result.toNumber(), 'ether')
			cache.set('balance', balance)
		} else
            console.log(error)
	  })
}

