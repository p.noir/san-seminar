import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Eat from './Eat'
import Reservation from './Reservation'
import EditMeal from './EditMeal'
import Cook from './Cook'

class Main extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={Eat}></Route>
                <Route exact path='/eat' component={Eat}></Route>
                <Route exact path='/meal/:number' component={Reservation}></Route>
                <Route exact path='/meal/:number/edit' component={EditMeal}></Route>
                <Route exact path='/cook' component={Cook}></Route>
            </Switch>
        )
    }
}

export default Main
