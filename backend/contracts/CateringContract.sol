pragma solidity ^0.4.23;

contract CateringContract {
    //address cook;
    //TODO: check what is this uint? the current ETH balance or some kind of IDs?
    //mapping (address => uint) public balances;
    //mapping (address => uint) public cookBalances;
    //mapping (address => uint) public eaterBalances;
    
    //List of eaters address for each meal
    mapping (uint => address[]) public listOfReservation;
    
    //Number of reservation for each MealCookChanged
    mapping (uint => uint) public numberOfReservation;
    
    struct Meal { 
        uint mealId;
        address cook;
        string  title;
        string  description;
        string  location;
        uint256 time;
        uint    capacity;
        uint    price;
        address[] reservations;
    }
    
    
    Meal[] public meals;

    event MealCookChanged(
        uint index
    );
    
    event MealPriceChanged(
        uint index,
        uint price
    );
    
    event MealAvailabilityChanged(
        uint index,
        uint price    
    );
    
    event MealReservationChange (
        uint mealId,
        address[] reservations
    );
    
    event MealChanged(
        uint mealId,
        address cook,
        string title, 
        string description, 
        string location,
        uint capacity
    );
    
    event MealAdded(
        uint mealId,
        address cook,
        string title, 
        string description, 
        string location,
        uint256 time, 
        uint capacity, 
        uint price,
        address [] reserve
    );
    
    
    
    // This function returns the total number of reservation a meal has received so far
    function totalNumOfReservation(uint mealId) view public returns (uint) {
        require(mealId <= getLastMealId());
        return numberOfReservation[mealId];
    }
    
    
    // This function returns the listof reservation a meal has received so far
    function totalReservation(uint mealId) view public returns (address[]) {
        require(mealId <= getLastMealId());
        return listOfReservation[mealId];
    }
    
    
    function getMealsLength() view public returns (uint) {
        return meals.length;
    }
    
    
    /**
     * Add a new meal
     * args:
     *  - title         the title of dishes,
     *  - description   the description of the dish,
     *  - location      the place of the meal
     *  - time          the time when the meal will be ready in Unix format
     *  - capacity      the number of available dishes
     *  - price         how much the total meal costs
     * 
    **/
    function addMeal(string title, string description, string location,
        uint256 time, uint capacity, uint price) public {
        uint newMealId = getLastMealId() + 1;
        //getLastIdByCook(msg.sender) + 1;
        //address[] memory reserve= new address[](capacity);
        Meal memory meal = Meal (newMealId, msg.sender, title, description, location, time, capacity, price, new address[](capacity));
        //providedMealByCook[msg.sender].push(meal);
        //providedMealByCook[msg.sender][mealId] = (meal);
        meals.push(meal); 
        emit MealAdded(meal.mealId, msg.sender, meal.title, meal.description, meal.location, meal.time, meal.capacity, meal.price, meal.reservations);
    }
    
    /**
     * 
     * 
     **/
    function changeMeal(uint mealId, string title, string description, string location,
        uint capacity) public {
        //Meal memory meal = providedMealByCook[msg.sender][mealId];
        Meal memory meal = meals[mealId];
        require(msg.sender == meal.cook);
        meal.title = title;
        meal.description = description;
        meal.location = location;
        meal.capacity = capacity;
        //providedMealByCook[msg.sender][mealId] = meal;
        meals[mealId] = meal;
        emit MealChanged(meal.mealId, msg.sender, meal.title, meal.description, meal.location, meal.capacity);
    }
    
    /**
     * 
     * 
     **/ 
    function getReservations(uint mealId) public view returns (address[]) {
        Meal memory meal = meals[mealId];
        require(msg.sender == meal.cook);
        return meal.reservations;
    }
    
    /**
     * 
     * 
     **/
    function addReservations(uint mealId) public payable {
        //Meal memory meal = meals[mealId];
        Meal storage meal = meals[mealId];
        //require(meal.capacity <= meal.reservations.length && msg.value >= meal.price);
        require(msg.value >= meal.price && totalNumOfReservation(mealId)<meal.capacity);

        //add check for msg.sender != meal.cook to prevent self buyer
        //TODO: Transaction the ETH amount
        //when successful Transaction
        meal.cook.transfer(msg.value);
        //--cookBalances[meal.cook] += msg.value;
        //--eaterBalances[msg.sender] -= msg.value;
        //uint newReservationId = getLastReservationId(mealId)+1;
        //meal.reservations[newReservationId]=0x9C4dC2613aCF4DD1F2E9e522811fBb8E6EA3f90e;
        //meal.reservations[meal.reservations.length] = msg.sender;
        
        listOfReservation[mealId].push(msg.sender);
        //meal.reservations.push(msg.sender);
        meal.reservations[numberOfReservation[mealId]]=msg.sender;
        numberOfReservation[mealId] += 1;
        //meal.capacity = meal.capacity-1;
        meals[mealId] = meal;
        emit MealReservationChange(mealId, meal.reservations);
        
    }
    
 /*   
 
 
     
     function buyPlot(uint index) public payable {
            Plot storage plot = plots[index];
            
            require(msg.sender != plot.owner && plot.forSale && msg.value >= plot.price);
            
            if(plot.owner == 0x0) {
                balances[owner] += msg.value;
            }else {
                balances[plot.owner] += msg.value;
            }
            
            plot.owner = msg.sender;
            plot.forSale = false;
            
            emit PlotOwnerChanged(index);
        }
    function takeOffMarket(uint index) public {
        Meal storage meal = meals[index];
        
        require(msg.sender == meal.cook);
        
        meal.forSale = false;
        emit MealAvailabilityChanged(index, meal.price, false);
    }
    
    
    function buyMeal(uint index) public payable {
        Meal storage meal = meals[index];
        
        require(msg.sender != meal.cook && meal.forSale && msg.value >= meal.price);
        
        if(meal.cook == 0x0) {
            balances[cook] += msg.value;
        }else {
            balances[meal.cook] += msg.value;
        }
        
        meal.cook = msg.sender;
        meal.forSale = false;
        
        emit MealCookChanged(index);
    }
    
    function withdrawFunds() public {
        address payee = msg.sender;
          uint payment = balances[payee];
    
          require(payment > 0);
    
          balances[payee] = 0;
          require(payee.send(payment));
    }
    
    
    function destroy() payable public {
        require(msg.sender == cook);
        selfdestruct(cook);
    }
    
*/
/*
    function getLastIdByCook(address cookAddress) constant private returns (uint lastMealId) {
        return providedMealByCook[cookAddress].length - 1;
    }
  */  
    function getLastMealId() constant private returns (uint lastMealId) {
        return meals.length - 1;
    }
    function getLastReservationId(uint mealId) constant private returns (uint lastReservationId) {
        return meals[mealId].reservations.length - 1;
    }

    function stringToBytes32(string memory source) pure private returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
    
    function strConcat(string _a, string _b, string _c, string _d, string _e) internal pure returns (string){
        bytes memory _ba = bytes(_a);
        bytes memory _bb = bytes(_b);
        bytes memory _bc = bytes(_c);
        bytes memory _bd = bytes(_d);
        bytes memory _be = bytes(_e);
        string memory abcde = new string(_ba.length + _bb.length + _bc.length + _bd.length + _be.length);
        bytes memory babcde = bytes(abcde);
        uint k = 0;
        for (uint i = 0; i < _ba.length; i++) babcde[k++] = _ba[i];
        for (i = 0; i < _bb.length; i++) babcde[k++] = _bb[i];
        for (i = 0; i < _bc.length; i++) babcde[k++] = _bc[i];
        for (i = 0; i < _bd.length; i++) babcde[k++] = _bd[i];
        for (i = 0; i < _be.length; i++) babcde[k++] = _be[i];
        return string(babcde);
    }
    
    function strConcat(string _a, string _b, string _c, string _d) internal pure returns (string) {
        return strConcat(_a, _b, _c, _d, "");
    }
    
    function strConcat(string _a, string _b, string _c) internal pure returns (string) {
        return strConcat(_a, _b, _c, "", "");
    }
    
    function strConcat(string _a, string _b) internal pure returns (string) {
        return strConcat(_a, _b, "", "", "");
    }
}